import { Tile, TileType } from "./Tile";
import { choose, shuffle } from "./Utils";
import { Level } from "./LevelManager";
import { Vec2 } from "cc";
import { BoosterType } from "./Boosters/Booster";
import { BoosterBomb } from "./Boosters/BoosterBomb";
import { BoosterFish } from "./Boosters/BoosterFish";
import { BoosterRocket } from "./Boosters/BoosterRocket";
import { BoosterMegaBomb } from "./Boosters/BoosterMegaBomb";

export enum BoardState {
    INIT,       // Инициализация доски
    IDLE,       // Ожидание указания к действию
    PROCESS,    // Действия на доске
    DROP,       // Гравитация
    FILL,       // Спавн новых тайлов в свободные области
    FINISHED,   // Игра завершена (закончились ходы)
}

export type MovedTile = {
    tileID: number,
    oldIndex: number,
    newIndex: number,
}

/** Минимальное число смежных одинаковых тайлов для матча */
const MIN_MATCH = 2;

const SHUFFLES_AMOUNT = 3;

const BOOSTER_PROTOTYPES = {
    [BoosterType.Bomb]: BoosterBomb,
    [BoosterType.Fish]: BoosterFish,
    [BoosterType.Rocket]: BoosterRocket,
    [BoosterType.MegaBomb]: BoosterMegaBomb,
}

/**
 * Класс представляет собой модель игровой доски.
 */
export class Board {
    /** Уникальный идентификатор, назначаемый тайлам при их создании */
    private nextTileID: number = 1;

    /** Тайлы на доске. Если тайл в позиции отсутствует, то там записан null */
    private board: Tile[];

    /** Ширина доски (в тайлах) */
    private boardWidth: number;

    /** Высота доски (в тайлах) */
    private boardHeight: number;

    /** Размер доски (чтобы не считать каждый раз) */
    private boardSize: number;

    /** Текущее состояние модели */
    private boardState: BoardState = BoardState.INIT;

    /** Количество доступных ходов */
    private moves: number;

    /** Сколько нужно набрать очков для победы */
    private targetScores: number;

    /** Конфиг загружаемого уровня */
    private level: Level;

    /** Сколько очков набрано в текущей сессии */
    private collectedScores = 0;

    /** Количество доступных перемешиваний доски */
    private shuffles = SHUFFLES_AMOUNT;

    constructor(level: Level) {
        console.log("[Board] Started");
        this.level = level;

        this.boardWidth = level.width;
        this.boardHeight = level.height;
        this.boardSize = this.boardWidth * this.boardHeight;

        this.moves = level.moves;
        this.targetScores = level.targetScores;

        this.board = new Array(level.width * level.height).fill(null);

        this.setBoardState(BoardState.IDLE);

        this.fillBoardOnStart();
    }

    /**
     * Получение ширины доски (в тайлах)
     * @returns Ширина доски
     */
    getBoardWidth(): number {
        return this.boardWidth;
    }

    /**
     * Получение высоты доски (в тайлах)
     * @returns Высота доски
     */
    getBoardHeight(): number {
        return this.boardHeight;
    }

    /**
     * Поменять текущий режим модели
     * @param state Новое состояние
     */
    private setBoardState(state: BoardState) {
        this.boardState = state;
        // console.log("[Board] New state: ", BoardState[state]);
    }

    /** Получить следующий уникальный ID для нового тайла */
    private getTileNextID(): number {
        return this.nextTileID++;
    }

    /** Получить текущий режим модели */
    getBoardState(): BoardState {
        return this.boardState;
    }

   /**
    * Получить клетку по указанным координатам
    * @param ix Индекс по оси X
    * @param iy Индекс по оси Y
    * @returns Тайл в указанной позиции, либо null, если позиция пуста
    */
    getCellByPos(ix: number, iy: number): Tile {
        if (ix < 0 || iy < 0 || ix >= this.boardWidth || iy >= this.boardHeight) {
            return null;
        }

        let index = this.getIndexByPos(ix, iy);
        return this.board[index];
    }

    /**
     * Получить клетку по указанному индексу
     * @param index Индекс клетки
     * @returns Тайл в указанной позиции, либо null, если позиция пуста
     */
    getCellByIndex(index: number): Tile {
        return this.board[index];
    }

    /** Задать значение клетки с указанным индексом */
    setCellByIndex(index: number, tile: Tile) {
        this.board[index] = tile;
        tile.index = index;
    }

    /** Очистить клетку с указанным индексом */
    clearCellByIndex(index: number) {
        this.board[index] = null;
    }

    /** Поменять два тайла местами */
    swapCells(x1: number, y1: number, x2: number, y2: number) {
        let index1 = this.getIndexByPos(x1, y1);
        let index2 = this.getIndexByPos(x2, y2);
        let tile1 = this.getCellByIndex(index1);
        let tile2 = this.getCellByIndex(index2);
        if (tile1) {
            tile1.index = index2;
        }
        if (tile2) {
            tile2.index = index1;
        }
        [this.board[index1], this.board[index2]] = [this.board[index2], this.board[index1]];
    }

    /** Получение доски в текстовом виде (используется для отладки) */
    boardToString(richText?: boolean): string {
        let result = "";
        for (let index: number = 0; index < this.boardSize ; index++) {
            let t: Tile = this.getCellByIndex(index);
            let s = "        ";
            if (t != null) {

                s = t.toStringDetailed(richText) + " ";
            }
            result += s;

            if (index > 0 && index % this.boardWidth == this.boardWidth - 1) {
                result += "\n";
            }
        }

        return result;
    }

    /** Отладочный вывод доски в консоль */
    printBoard() {
        let result = this.boardToString();
        console.log(result);
    }

    /** Заполнение доски */
    fillBoardOnStart() {
        for (let item of this.level.items) {
            let typeID = item.typeID;
            if (typeID == null) {
                typeID = choose(this.level.randomTiles);
            }
            let tile = new Tile(this.getTileNextID(), typeID);
            this.setCellByIndex(item.index, tile);
        }
    }

    /** Проверка поля на доступные ходы */
    checkForMatches(): boolean {
        let checkedTiles: boolean[] = Array<boolean>(this.boardSize).fill(false);
        for (let i = 0; i < this.boardSize; i++) {
            let pos = this.getPosByIndex(i);
            if (!checkedTiles[i] && this.getCellByIndex(i) != null) {
                let tiles = this.getMatch(pos.x, pos.y);
                if (tiles.length >= MIN_MATCH) {
                    return true;
                } else {
                    for (let tile of tiles) {
                        checkedTiles[tile.index] = true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Получение группы одинаковых соприкасающихся тайлов, начиная с заданной позиции
     * @param x Индекс тайла по горизонтали
     * @param y Индекс тайла по вертикали
     */
    getMatch(x: number, y: number) {
        const boardWidth: number = this.boardWidth;
        const boardHeight: number = this.boardHeight;

        let group: Tile[] = [];
        let checkedIndices: boolean[] = Array(this.boardSize).fill(false);

        let indicesToCheck: number[] = [];
        let self = this;

        let index = self.getIndexByPos(x, y);
        let cell = this.getCellByPos(x, y);
        if (cell == null) {
            console.log(`Tile not found. Pos: ${x}, ${y}. Index: ${index}`);
        }
        let requiredType: TileType = cell.typeID;

        function processCell(self: Board, x: number, y: number) {
            // Если есть выход за пределы поля
            if (x < 0 || x > boardWidth - 1 || y < 0 || y > boardHeight - 1) {
                return;
            }

            let index = self.getIndexByPos(x, y);

            // Если позиция уже проверялась
            if (checkedIndices[index]) {
                return;
            }

            checkedIndices[index] = true;           // Тайл проверен

            let tile = self.getCellByIndex(index);
            if (tile != null && tile.typeID == requiredType) {
                indicesToCheck.push(index);
            }
        }

        // Поиск подходящих соседних тайлов
        processCell(this, x, y);
        while (indicesToCheck.length > 0) {
            let index = indicesToCheck.shift();
            let pos = this.getPosByIndex(index);

            let tile: Tile = this.getCellByIndex(index);
            group.push(tile);

            processCell(this, pos.x - 1, pos.y);    // Проверка тайла слева
            processCell(this, pos.x + 1, pos.y);    // Проверка тайла справа
            processCell(this, pos.x, pos.y - 1);    // Проверка тайла сверху
            processCell(this, pos.x, pos.y + 1);    // Проверка тайла снизу
        }

        return group;
    }

    /**
     * Отработать прикосновение по указанной клетке
     * @param x Индекс клетки по оси X
     * @param y Индекс клетки по оси Y
     * @returns Количество тайлов в матче
     */
    touchPos(x: number, y: number): number {
        let tile: Tile = this.getCellByPos(x, y);
        if (tile != null) {
            let group = this.getMatch(x, y);
            let boosterData = tile.getBoosterData()
            if (boosterData != null) {
                console.log("USE BOOSTER! ", boosterData);
                // tile.onAction();
            } else if (group.length >= MIN_MATCH) {
                let collected_amount: number = group.length;
                this.collectTiles(group);
                return collected_amount;
            }
        }

        return 0;
    }

    /**
     * Собрать указанные тайлы
     * @param group Массив тайлов, которые необходимо собрать
     */
    collectTiles(group: Tile[]) {
        this.setBoardState(BoardState.PROCESS);
        group.forEach((tile: Tile) => {
            this.collectedScores += tile.scores;
            this.clearCellByIndex(tile.index);
        }, this)
    }

    /**
     * Применить гравитацию к тайлам на доске
     * @returns Список перемещённых тайлов
     */
    applyGravity(): MovedTile[] {
        this.setBoardState(BoardState.DROP);
        let movedTiles: MovedTile[] = [];
        for (let x = 0; x < this.boardWidth; x++) {
            let empty_y = null;
            for (let y = this.boardHeight - 1; y >= 0; y--) {
                let tile = this.getCellByPos(x, y);
                if (tile == null) {
                    if (empty_y == null) {
                        empty_y = y;
                    }
                } else if (empty_y != null) {
                    let upperTile = this.getCellByPos(x, y);
                    if (upperTile != null) {
                        let oldIndex = upperTile.index;
                        this.swapCells(x, y, x, empty_y--);
                        let t: MovedTile = {
                            tileID: upperTile.uniqueID,
                            newIndex: upperTile.index,
                            oldIndex,
                        }
                        movedTiles.push(t);
                    }
                }
            }
        }

        return movedTiles;
    }

    /**
     * Получение 2D координаты на доске по индексу (1D → 2D)
     * @param index
     * @returns
     */
    getPosByIndex(index: number): Vec2 {
        let x = index % this.boardWidth;
        let y = Math.floor(index / this.boardWidth);
        return new Vec2(x, y);
    }

    /**
     * Получение индекса по 2D координате (2D → 1D)
     * @param x
     * @param y
     * @returns
     */
    getIndexByPos(x: number, y: number): number {
        return y * this.boardWidth + x;
    }

    /**
     * Заполнение пустот на доске новыми тайлами. После обработки данных (обновления вида, например),
     * требуется вызвать onFilled()
     * @returns Список новых тайлов
     */
    fillBoard() {
        let newTiles: Tile[] = [];
        for (let i = 0; i < this.boardSize; i++) {
            let tile = this.getCellByIndex(i);
            if (tile == null) {
                let typeID = choose(this.level.randomTiles);
                let tile = new Tile(this.getTileNextID(), typeID);
                this.setCellByIndex(i, tile);
                newTiles.push(tile);
            }
        }

        return newTiles;
    }

    /**
     * Завершение состояния после заполнения доски. Требуется для того, чтобы можно было выполнить анимацию
     * заполнения доски новыми тайлами
     */
    onFilled() {
        let isFinished = (this.moves == 0) || (this.getScores() >= this.getRequiredScores());
        this.setBoardState(isFinished ? BoardState.FINISHED : BoardState.IDLE);
    }

    /**
     * Получить количество доступных ходов
     * @returns Количество доступных ходов
     */
    getMoves(): number {
        return this.moves;
    }

    /**
     * Использовать ход
     * @returns true, если ход использовался; false, если доступных ходов нет
     */
    useMove(): boolean {
        if (this.moves > 0) {
            this.moves--;
            return true;
        }

        return false;
    }

    /**
     * Получить количество набранных очков
     * @returns Количество набранных очков
     */
    getScores(): number {
        return this.collectedScores;
    }

    /**
     * Получить количество очков, требуемых для победы
     * @returns Сколько очков нужно набрать, чтобы победить
     */
    getRequiredScores(): number {
        return this.targetScores;
    }

    /**
     * Перемешивание массива случайным образом
     * @param array
     */
    shuffleBoard(): boolean {
        if (this.shuffles > 0) {
            this.shuffles--;

            // Перемешать
            shuffle(this.board);

            // Обновить индексы тайлов в соответствие с реальной позицией
            for (let i = 0; i < this.board.length; i++) {
                let tile = this.getCellByIndex(i);
                if (tile != null) {
                    tile.index = i;
                }
            }
            return true;
        }

        this.setBoardState(BoardState.FINISHED);

        return false;
    }

    /**
     * Применить бустер в указанной позиции
     * @param boosterType Тип бустера
     * @param ix Индекс клетки по оси X
     * @param iy Индекс клетки по оси Y
     * @param options? Дополнительные опции
     * @returns Список тайлов, на которые производится воздействие, либо null, если применить невозможно
     */
    applyBooster(boosterType: BoosterType, ix: number, iy: number, options?: any): Tile[] {
        let booster = new BOOSTER_PROTOTYPES[boosterType](options);
        return booster.apply(this, ix, iy);
    }

    /**
     * Проверить, нужно ли генерировать бонус за сбор определённого количества тайлов
     * @param tilesAmount Сколько было собрано тайлов
     * @returns Тип бустера, либо null
     */
    onGroupCollected(tilesAmount: number): BoosterType {
        type BoosterScores = {
            scores: number,
            booster: BoosterType[],
        }

        // TODO: Такое должно работать с конфигом
        const amount: BoosterScores[] = [
            { scores: 4, booster: [ BoosterType.Bomb ] },
            { scores: 8, booster: [ BoosterType.Rocket, BoosterType.Fish ] },
            { scores: 15, booster: [ BoosterType.MegaBomb ] },
        ].sort((a, b) => (a.scores > b.scores ? -1: 1));

        for (let data of amount) {
            if (tilesAmount >= data.scores) {
                return choose(data.booster);
            }
        }

        return null;
    }

    /**
     * Создать бустер в указанной позиции
     * @param ix Индекс клетки по оси X
     * @param iy Индекс клетки по оси Y
     * @param boosterType Тип создаваемого бустера
     * @returns Созданный тайл
     */
    createBooster(ix: number, iy: number, boosterType: BoosterType): Tile {
        console.log(`Create booster: ${BoosterType[boosterType]}, ix: ${ix}, iy: ${iy}`);

        const types = {
            [BoosterType.Bomb]: TileType.Bomb,
            [BoosterType.Fish]: TileType.Fish,
            [BoosterType.Rocket]: TileType.Rocket,
            [BoosterType.MegaBomb]: TileType.MegaBomb,
        }

        let tileType = types[boosterType];
        let tile = new Tile(this.getTileNextID(), tileType);
        let index = this.getIndexByPos(ix, iy);
        this.setCellByIndex(index, tile);
        return tile;
    }

    /**
     * Проверка, является ли указанный тайл бустером
     * @param tileIndex
     * @returns true, если тайл является бустером, иначе false
     */
    isBooster(tileIndex: number): boolean {
        let tile = this.getCellByIndex(tileIndex);
        return (tile != null && tile.boosterType != null);
    }

    /**
     * Найти тайл по его ID. Сейчас используется перебор, но в реальном проекте лучше использовать look-up таблицы.
     * @param tileID
     * @returns Тайл, либо null, если тайл не найден.
     */
    getTileByID(tileID: number): Tile {
        for (let i = 0; i < this.boardSize; i++) {
            let tile = this.getCellByIndex(i);
            if (tile != null && tile.uniqueID == tileID) {
                return tile;
            }
        }

        return null;
    }

    /**
     * Проверка, есть ли на доске бустеры
     * @returns true, если бустеры на доске есть, иначе false
     */
    isThereBooster() {
        for (let i = 0; i < this.boardSize; i++) {
            let tile = this.getCellByIndex(i);
            if (tile != null && tile.isBooster()) {
                return true;
            }
        }

        return false;
    }
}
