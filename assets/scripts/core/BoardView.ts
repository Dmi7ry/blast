import { _decorator, Component, Node, instantiate, Prefab, assert, Sprite, RichText, Vec2, Label, Vec3, Layers, ProgressBar } from 'cc';
import { Board, MovedTile, BoardState } from './Board';
import { TileView } from './TileView';
import { Tile, TileType } from './Tile';
import { isDebugMode } from './Utils';
const { ccclass, property } = _decorator;

const TILE_WIDTH = 171;
const TILE_HEIGHT = 192;
const START_POS_X = 83;
const START_POS_Y = -90;

const TWEEN_DURATION = 0.3;   // Скорость перемещения тайлов на доске (секунды)

const TILE_SPRITES = {
    [TileType.Blue]: "blue",
    [TileType.Green]: "green",
    [TileType.Purple]: "purple",
    [TileType.Red]: "red",
    [TileType.Yellow]: "yellow",
    [TileType.Fish]: "fish",
    [TileType.Rocket]: "rocket",
    [TileType.Bomb]: "bomb",
    [TileType.MegaBomb]: "atomic-bomb",
}

@ccclass('BoardView')
export class BoardView extends Component {
    private board: Board = null;
    private boardWidth: number;
    private boardHeight: number;
    private boardSize: number;

    private previousDebugMode = false;

    @property(Prefab)
    private tileViewPrefab: Prefab;

    @property(Node)
    private tilesArea: Node;

    @property(RichText)
    private boardRichText: RichText;

    @property(Label)
    private boardStatusLabel: Label;

    @property(Label)
    private scoresCounterLabel: Label;

    @property(Label)
    private movesCounterLabel: Label;

    @property(ProgressBar)
    private scoresProgressBar: ProgressBar;

    private tileViewsArray: TileView[] = [];

    private cleanupMode = false;    // Режим очистки доски (для эффекта плавного исчезновения тайлов)

    start() {
        console.log("[BoardView] Started");
    }

    update(deltaTime: number) {
        this.updateBoardStateLabel();
        this.checkDebugMode();
        this.updateCounters();
        this.checkCleanup();
        this.updateScoresProgress();
    }

    getTilesArea(): Node {
        return this.tilesArea;
    }

    setBoard(board: Board) {
        this.board = board;
        this.boardWidth = board.getBoardWidth();
        this.boardHeight = board.getBoardHeight();
        this.boardSize = this.boardWidth * this.boardHeight;

        for (let i = 0; i < this.boardSize; i++) {
            let tile: Tile = this.board.getCellByIndex(i);
            let pos = this.board.getPosByIndex(i);
            this.addTile(pos.x, pos.y, tile.typeID, tile.uniqueID);
        }
    }

    addTile(ix: number, iy: number, tileType: TileType, tileID: number) {
        const tileViewNode: Node = instantiate(this.tileViewPrefab);
        let tileView: TileView = tileViewNode.getComponent("TileView") as TileView;
        this.tilesArea.addChild(tileViewNode);
        tileView.setPosition(START_POS_X + TILE_WIDTH * ix, START_POS_Y - TILE_HEIGHT * iy);
        tileView.setTileID(tileID);
        let index = this.board.getIndexByPos(ix, iy);
        tileView.setTileIndex(index);

        let spriteName: string = TILE_SPRITES[tileType];
        assert(typeof(spriteName) == "string", "Unknown tile type: " + tileType);

        var spr = tileViewNode.getComponent("cc.Sprite") as Sprite;
        spr.changeSpriteFrameFromAtlas(spriteName);

        this.tileViewsArray.push(tileView);

        return tileView;
    }

    /** Получить TileView по координатам (пиксели, относительно левого верхнего угла родителя — TilesArea)*/
    getTileViewByPos(dx: number, dy: number): TileView {
        let index = this.getIndexByCoord(dx, dy);
        return this.getTileViewByIndex(index);
    }

    /**
     * Найти TileView, связанный с тайлом с указанным индексом.
     * Сейчас используется перебор, но в реальном проекте лучше использовать look-up таблицы.
     * @param index
     * @returns
     */
    getTileViewByIndex(index: number): TileView {
        for (let tileView of this.tileViewsArray) {
            if (tileView.getTileIndex() == index) {
                return tileView;
            }
        }

        return null;
    }

    /**
     * Найти TileView, связанный с тайлом с указанным ID.
     * Сейчас используется перебор, но в реальном проекте лучше использовать look-up таблицы.
     * @param tileID
     * @returns
     */
    findTileViewByTileID(tileID: number): TileView {
        for (let tileView of this.tileViewsArray) {
            if (tileView.getTileID() == tileID) {
                return tileView;
            }
        }

        return null;
    }

    destroyTileViewByIndex(index: number) {
        let tileView = this.getTileViewByIndex(index);
        assert(tileView != null, `TileView not found. Index: ${index}`);
        let i = this.tileViewsArray.indexOf(tileView);
        this.tileViewsArray.splice(i, 1);
        tileView.onDestroy();
        setTimeout(() => {
            tileView.node.destroy();
        }, 500)
    }

    onTileMoved(movedTile: MovedTile, isFell: boolean) {
        let tileView = this.getTileViewByIndex(movedTile.oldIndex);
        if (tileView != null && this.board.getTileByID(movedTile.tileID) != null) {
            let pos = this.board.getPosByIndex(movedTile.newIndex);
            let oldPos = this.board.getPosByIndex(movedTile.oldIndex);
            let delta = 1 //isFell ? Math.abs(pos.y - oldPos.y) : 1;    // На сколько клеток упал тайл

            let x = START_POS_X + TILE_WIDTH * pos.x;
            let y = START_POS_Y - TILE_HEIGHT * pos.y;
            tileView.moveTo(x, y, TWEEN_DURATION * delta, isFell);
            tileView.setTileIndex(movedTile.newIndex);
        }
    }

    onBoardShuffled() {
        // Составление lookup таблицы tileID → индекс на доске (для быстрого поиска)
        let indices = {};
        for (let i = 0; i < this.boardSize; i++) {
            let tile = this.board.getCellByIndex(i);
            if (tile != null) {
                indices[tile.uniqueID.toString()] = i;
            }
        }

        // Перемещение
        for (let tileView of this.tileViewsArray) {
            let id = tileView.getTileID();
            let index = indices[id.toString()];
            let pos = this.board.getPosByIndex(index);

            let x = START_POS_X + TILE_WIDTH * pos.x;
            let y = START_POS_Y - TILE_HEIGHT * pos.y;

            tileView.moveTo(x, y, TWEEN_DURATION * 2, false);
            tileView.setTileIndex(index);
        }
    }

    /**
     * Обновление отладочного вывода
     */
    updateLabelBoard() {
        this.boardRichText.string = this.board.boardToString(true);
    }

    getCoordByIndex(index: number): Vec2 {
        let x = (index % this.boardWidth) * TILE_WIDTH;
        let y = Math.floor(index / this.boardWidth) * TILE_HEIGHT;
        return new Vec2(x, y);
    }

    getIndexByCoord(x: number, y: number): number {
        x = Math.min(x, this.boardWidth * TILE_WIDTH - 1);
        y = Math.min(y, this.boardHeight * TILE_HEIGHT - 1);

        let ix = Math.floor(x / TILE_WIDTH);
        let iy = Math.floor(y / TILE_HEIGHT);
        return iy * this.boardWidth + ix;
    }

    updateBoardStateLabel() {
        let state = this.board.getBoardState();
        let text = "Board State: " + BoardState[state];
        let matches = this.board.checkForMatches();
        text += `\nMatches: ${matches}`;

        this.boardStatusLabel.string = text;
    }

    onTilesSpawned(tiles: Tile[]) {
        for (let tile of tiles) {
            let pos = this.board.getPosByIndex(tile.index);
            let delta_iy = this.boardHeight - pos.y;
            let delta_y = (delta_iy) * TILE_HEIGHT;
            let tileView: TileView = this.addTile(pos.x, pos.y, tile.typeID, tile.uniqueID);
            let node = tileView.node;
            let targetPos = node.position.clone();
            node.setPosition(new Vec3(node.position.x, node.position.y + delta_y, 0));
            tileView.moveTo(targetPos.x, targetPos.y, TWEEN_DURATION, false);
        }
    }

    onBoosterSpawned(tile: Tile) {
        let pos = this.board.getPosByIndex(tile.index);
        this.addTile(pos.x, pos.y, tile.typeID, tile.uniqueID);
    }

    checkDebugMode() {
        let mode = isDebugMode();
        if (mode != this.previousDebugMode) {
            this.previousDebugMode = mode;

            this.boardRichText.node.layer = mode ? Layers.Enum.UI_2D : Layers.Enum.NONE;
            this.boardStatusLabel.node.layer = mode ? Layers.Enum.UI_2D : Layers.Enum.NONE;
        }
    }

    updateCounters() {
        this.movesCounterLabel.string = this.board.getMoves().toString();
        this.scoresCounterLabel.string = this.board.getScores().toString();
    }

    cleanupBoard() {
        this.cleanupMode = true;
    }

    checkCleanup() {
        if (this.cleanupMode) {
            if (this.tileViewsArray.length > 0) {
                let tile = this.tileViewsArray.pop()
                tile.onDestroy()
            }
        }
    }

    updateScoresProgress() {
        let currentScores = this.board.getScores();
        let targetScores = this.board.getRequiredScores();

        let k = currentScores == 0 ? 0 : Math.max(0.06, currentScores / targetScores);

        let delta = k - this.scoresProgressBar.progress;
        this.scoresProgressBar.progress += delta / 2;
    }
}
