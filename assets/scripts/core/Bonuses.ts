import { _decorator, Component, Label, Node, Sprite, EventTouch, Vec3, UIOpacity, tween } from 'cc';
import { BoosterType } from './Boosters/Booster';
const { ccclass, property } = _decorator;

type BonusPanel = {
    icon: Sprite,
    amountLabel: Label,
}

type BoosterConfig = {
    id: number,
    amount: number,
    sprite: string,
}

/**
 * Награды, доступные игроку. При сменен уровня не обновляются.
 */
const START_BOOSTERS: BoosterConfig[] = [
    { id: BoosterType.Bomb, amount: 2, sprite: "bomb" },
    { id: BoosterType.Rocket, amount: 1, sprite: "rocket" },
    { id: BoosterType.Fish, amount: 1, sprite: "fish" },
]

/**
 * Бонусы, доступные для применения на уровне.
 * Предполагается, что их либо выбирает игрок перед началом уровня, либо они выдаются в качестве наград.
 * В данный момент они просто захардкожены в табличке START_BOOSTERS и выдаются не на каждый уровень, а на всю игру
 */
@ccclass('Bonuses')
export class Bonuses extends Component {
    @property(Node)
    private bonusesRoot: Node;

    /** Номер выбранного в данный момент бонуса, либо null, если ничего  не выбрано */
    private selectedBonus: number = null;

    private panels: BonusPanel[] = [];

    start() {
        console.log("[Bonuses] Started");
        this.initializePanels();
    }

    initializePanels() {
        let len = Math.min(START_BOOSTERS.length, 3);
        for (let i = 0; i < len; i++) {
            let panel = this.bonusesRoot.getChildByName("BonusPanel" + (i + 1)) as Node;
            let data = {
                icon: panel.getChildByName("Icon").getComponent(Sprite) as Sprite,
                amountLabel: panel.getChildByName("AmountLabel").getComponent(Label) as Label,
            }
            this.panels.push(data);

            let config = START_BOOSTERS[i];

            data.icon.changeSpriteFrameFromAtlas(config.sprite);
            data.amountLabel.string = config.amount.toString();
        }
    }

    onClick(event: EventTouch, panelIndex: string) {
        let index = parseInt(panelIndex);
        if (START_BOOSTERS[index].amount > 0) {
            let current = this.getSelected();
            let i = (current == null || current != index) ? index : null;
            this.select(i);
            this.updatePanels();
        }
    }

    updatePanels() {
        let current = this.getSelected();
        for (let i = 0; i < this.panels.length; i++) {
            let config = START_BOOSTERS[i];
            this.panels[i].amountLabel.string = config.amount.toString();

            let panel = this.panels[i].icon.node.parent as Node;
            let isSelected = (i == current);
            let k = 1;
            let opacity = 255;
            if (current != null) {
                k = isSelected ? 1.1 : 0.9;
                opacity = isSelected ? 255 : 128;
            }

            if (config.amount < 1) {
                opacity = 128;
            }
            let duration = 0.05;
            tween(panel).to(duration, { scale: new Vec3(k, k, k) }).start();
            tween(panel.getComponent(UIOpacity)).to(duration, { opacity }, {  easing: "quartOut" }).start();
        }
    }

    /**
     * Требуется вызвать после применения бонуса. Обновляет доступное количество бустеров и состояние панелей.
     */
    onUsed() {
        let current = this.getSelected();
        START_BOOSTERS[current].amount--;
        this.select(null);
        this.updatePanels();
    }

    /**
     * Получить тип выбранного бонуса
     * @returns Тип выбранного бонуса
     */
    getSelectedType(): BoosterType {
        let current = this.getSelected();
        return current == null ? null : START_BOOSTERS[current].id;
    }

    /**
     * Выбрать бонус с указанным индексом (0 — левая панель, 1 — средняя, 2 — правая)
     * @param index
     */
    select(index: number) {
        this.selectedBonus = index;
        // console.log("Select bonus: ", this.selectedBonus);
    }

    getSelected(): number {
        return this.selectedBonus;
    }
}
