import { _decorator, Component, Node } from 'cc';
import { Tile } from '../Tile';
import { Board } from '../Board';
const { ccclass, property } = _decorator;

export enum BoosterType {
    Bomb, Fish, Rocket, MegaBomb
}

@ccclass('Booster')
export abstract class Booster extends Component {
    private typeID: BoosterType;

    protected options: any = {};

    /**
     * Применение бустера в указанной позиции на доске
     * @param board Доска
     * @param ix Индекс по оси X
     * @param iy Индекс по оси Y
     * @returns Список тайлов, на которые производится воздействие, либо null, если применить невозможно
     */
    abstract apply(board: Board, ix: number, iy: number): Tile[]

    getType(): BoosterType {
        return this.typeID;
    }

    setType(typeID: BoosterType) {
        this.typeID = typeID;
    }
}
