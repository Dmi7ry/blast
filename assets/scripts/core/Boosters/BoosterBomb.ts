import { _decorator } from 'cc';
import { Tile } from '../Tile';
import { Board } from '../Board';
import { Booster, BoosterType } from './Booster';
const { ccclass, property } = _decorator;

/**
 * Бомба. Удаляет тайлы в квадрате с заданным радиусом.
 * options.radius: Радиус взрыва. При radius равном 1, будет очищена область 3×3 клетки.
 */
@ccclass('BoosterBomb')
export class BoosterBomb extends Booster {
    options: any = {
        radius: 1,
    }

    constructor(options: any = {}) {
        super();
        this.setType(BoosterType.Bomb);
        this.options = { ...this.options, ...options };
        console.log("[Booster][Bomb] Created");
    }

    /**
     * Применение бустера в указанной позиции на доске
     * @param board Доска
     * @param ix Индекс по оси X
     * @param iy Индекс по оси Y
     * @returns Список тайлов, на которые производится воздействие, либо null, если применить невозможно
     */
    apply(board: Board, ix: number, iy: number): Tile[] {
        let affectedTiles: Tile[] = [];
        let size = this.options.radius;

        for (let x = ix - size; x <= ix + size; x++) {
            for (let y = iy - size; y <= iy + size; y++) {
                let tile = board.getCellByPos(x, y);
                if (tile != null) {
                    affectedTiles.push(tile);
                }
            }
        }

        return affectedTiles;
    }
}
