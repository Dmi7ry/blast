import { _decorator } from 'cc';
import { Booster, BoosterType } from './Booster';
import { Board } from '../Board';
import { Tile } from '../Tile';
const { ccclass, property } = _decorator;

/**
 * Рыба. Удаляет ряд тайлов.
 * options.radius: Сколько будет очищено дополнительных рядов сверху/снизу. При radius равном 1, будет очищено 3 ряда.
 */
@ccclass('BoosterFish')
export class BoosterFish extends Booster {
    options: any = {
        radius: 0,
    }

    constructor(options: any = {}) {
        super();
        this.setType(BoosterType.Fish);
        this.options = { ...this.options, ...options };
        console.log("[Booster][Fish] Created. Options: ", options);
    }

    apply(board: Board, ix: number, iy: number): Tile[] {
        let affectedTiles: Tile[] = [];
        let size = this.options.radius;

        for (let x = 0; x < board.getBoardWidth(); x++) {
            for (let y = iy - size; y <= iy + size; y++) {
                let tile = board.getCellByPos(x, y);
                if (tile != null) {
                    affectedTiles.push(tile);
                }
            }
        }

        return affectedTiles;
    }
}
