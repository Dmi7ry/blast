import { _decorator } from 'cc';
import { Booster, BoosterType } from './Booster';
import { Board } from '../Board';
import { Tile } from '../Tile';
const { ccclass, property } = _decorator;

/**
 * Мегабомба. Удаляет все тайлы на поле.
 */
@ccclass('BoosterMegaBomb')
export class BoosterMegaBomb extends Booster {
    constructor() {
        super();
        this.setType(BoosterType.MegaBomb);
        console.log("[Booster][MegaBomb] Created");
    }

    /**
     * Применение бустера в указанной позиции на доске
     * @param board Доска
     * @param ix Индекс по оси X
     * @param iy Индекс по оси Y
     * @returns Список тайлов, на которые производится воздействие, либо null, если применить невозможно
     */
    apply(board: Board, ix: number, iy: number): Tile[] {
        let affectedTiles: Tile[] = [];

        let size = board.getBoardWidth() * board.getBoardHeight();
        for (let i = 0; i < size; i++) {
            let tile = board.getCellByIndex(i);
            if (tile != null) {
                affectedTiles.push(tile);
            }
        }

        return affectedTiles;
    }
}
