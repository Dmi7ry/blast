import { _decorator } from 'cc';
import { Booster, BoosterType } from './Booster';
import { Board } from '../Board';
import { Tile } from '../Tile';
const { ccclass, property } = _decorator;

/**
 * Ракета. Удаляет столбец тайлов.
 * options.radius: Сколько будет очищено дополнительных столбцов слева/справа. При radius равном 1, будет очищено 3 столбца.
 */
@ccclass('BoosterRocket')
export class BoosterRocket extends Booster {
    options: any = {
        radius: 0,
    }

    constructor(options: any = {}) {
        super();
        this.setType(BoosterType.Rocket);
        this.options = { ...this.options, ...options };
        console.log("[Booster][Rocket] Created");
    }

    apply(board: Board, ix: number, iy: number): Tile[] {
        let affectedTiles: Tile[] = [];
        let size = this.options.radius;

        for (let x = ix - size; x <= ix + size; x++) {
            for (let y = 0; y < board.getBoardHeight(); y++) {
                let tile = board.getCellByPos(x, y);
                if (tile != null) {
                    affectedTiles.push(tile);
                }
            }
        }

        return affectedTiles;
    }
}
