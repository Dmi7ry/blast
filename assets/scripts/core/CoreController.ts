import { _decorator, Component, EventTouch, Node, UITransform, Vec3, Label, Layers, UIOpacity, tween, director } from 'cc';
import { Board, BoardState, MovedTile } from './Board';
import { BoardView } from './BoardView';
import { Tile } from './Tile';
import { LevelManager, Level } from './LevelManager';
import { isDebugMode, setDebugMode } from './Utils';
import { Bonuses } from './Bonuses';
import { BoosterType } from './Boosters/Booster';

const { ccclass, property } = _decorator;

const MIN_MATCH = 2;        // Минимальное число смежных одинаковых тайлов для матча

@ccclass('core_controller')
export class core_controller extends Component {
    @property(BoardView)
    private boardView: BoardView;

    @property(Node)
    private resultWindow: Node;

    @property(Node)
    private resultWindowBackground: Node;

    @property(Label)
    private resultWindowMessageLabel: Label;

    @property(Label)
    private levelCounterLabel: Label;

    @property(Node)
    private bonusesRoot;

    /** Модель игрового поля */
    private board: Board;

    /** Менеджер конфигов уровней */
    private levelManager: LevelManager;

    /** Область, где рисуются тайлы на доске */
    private tilesArea: Node;

    /** Текущий уровень */
    private level = 0;

    /** Ожидание клика по завершении уровня */
    private waitUserInputToEnd = false;

    /** Счётчик (задержка) на проверку поля на необходимость перемешивания */
    private totalDeltaTile = 0;

    /** Панель бонусов */
    private bonuses: Bonuses;

    start() {
        console.log("[CoreController] Started");

        this.hideResult();

        this.level = globalThis.ProjectDebugSettings.level;
        console.log("LEVEL: ", this.level);

        this.levelCounterLabel.string = this.level.toString();

        this.levelManager = new LevelManager();
        let level: Level = this.levelManager.getLevel(this.level);
        this.board = new Board(level);
        this.board.printBoard();

        this.boardView.setBoard(this.board);
        this.boardView.updateLabelBoard();

        this.tilesArea = this.boardView.getTilesArea();
        this.tilesArea.on(Node.EventType.TOUCH_START, this.onPressed, this);
        this.resultWindow.on(Node.EventType.TOUCH_START, this.onResultPressed, this);

        this.bonuses = this.bonusesRoot.getComponent("Bonuses") as Bonuses;
    }

    update(deltaTime: number) {
        this.totalDeltaTile += deltaTime;
        if (this.totalDeltaTile > 1) {
            this.totalDeltaTile = 0;

            this.checkForShuffle();
        }
    }

    onPressed(event: EventTouch) {
        if (event.target && this.board.getBoardState() == BoardState.IDLE) {
            let ui = event.target.getComponent("cc.UITransform") as UITransform;
            let pos = event.getUILocation();
            let relative_pos = ui?.convertToNodeSpaceAR(new Vec3(pos.x, pos.y, 0));
            let dx = Math.floor(relative_pos.x);
            let dy = -Math.floor(relative_pos.y);
            let tileIndex = this.boardView.getIndexByCoord(dx, dy);

            let selectedBonus = this.bonuses.getSelectedType();
            if (selectedBonus != null) {
                this.applyBooster(tileIndex, selectedBonus);
                this.bonuses.onUsed();
            } else if (this.board.isBooster(tileIndex)) {
                this.touchBooster(tileIndex);
            } else {
                this.touchPosition(tileIndex);
            }

            // this.applyBooster(tileIndex, BoosterType.MegaBomb);
            // this.applyBooster(tileIndex, BoosterType.Bomb);
            // this.applyBooster(tileIndex, BoosterType.Bomb, { radius: 2 });
            // this.applyBooster(tileIndex, BoosterType.Fish);
            // this.applyBooster(tileIndex, BoosterType.Fish, { radius: 1 } );
            // this.applyBooster(tileIndex, BoosterType.Rocket);
            // this.applyBooster(tileIndex, BoosterType.Rocket, { radius: 1 } );
        }
    }

    onResultPressed(event: EventTouch) {
        if (this.waitUserInputToEnd) {
            this.waitUserInputToEnd = false;
            if (globalThis.ProjectDebugSettings.level < this.levelManager.getMaxLevelID()) {
                globalThis.ProjectDebugSettings.level++;
                director.loadScene("core");
            } else {
                director.loadScene("meta");
            }
        }
    }

    /**
     * Отработать прикосновение в указанную позицию
     * @param tileIndex Позиция (индекс) на доске
     */
    touchPosition(tileIndex: number) {
        let tilePos = this.board.getPosByIndex(tileIndex);

        let group = this.board.getMatch(tilePos.x, tilePos.y);
        if (group.length >= MIN_MATCH) {
            if (this.board.useMove()) {
                group.forEach((tile: Tile) => {
                    this.boardView.destroyTileViewByIndex(tile.index);
                })
                this.board.collectTiles(group);

                let boosterType = this.board.onGroupCollected(group.length);
                if (boosterType != null) {
                    let tile = this.board.createBooster(tilePos.x, tilePos.y, boosterType);
                    this.boardView.onBoosterSpawned(tile);
                }

                this.applyGravity();
            }
        }
    }

    /**
     * Применить бустер в указанной позиции. После прикосновения применяется гравитация.
     * @param tileIndex Позиция (индекс) на доске
     * @param boosterType Тип бустера
     * @param options Параметры бустера
     */
    applyBooster(tileIndex: number, boosterType: BoosterType, options?: any) {
        this.applyBoosterBasic(tileIndex, boosterType, options);
        this.applyGravity();
    }

    /**
     * Применить бустер в указанной позиции.
     * @param tileIndex Позиция (индекс) на доске
     * @param boosterType Тип бустера
     * @param options Параметры бустера
     */
    applyBoosterBasic(tileIndex: number, boosterType: BoosterType, options?: any) {
        let tilePos = this.board.getPosByIndex(tileIndex);
        let tiles = this.board.applyBooster(boosterType, tilePos.x, tilePos.y, options);
        let boosters = [];
        if (tiles != null) {
            tiles.forEach((tile: Tile) => {
                if (tile.index != tileIndex && tile.isBooster()) {
                    let boosterData = tile.getBoosterData();
                    boosters.push({
                        index: tile.index,
                        boosterType: boosterData.boosterType,
                        boosterOptions: boosterData.boosterOptions,
                    });
                }

                let tileView = this.boardView.findTileViewByTileID(tile.uniqueID);
                if (tileView != null) {
                    this.boardView.destroyTileViewByIndex(tileView.getTileIndex());
                }
            })
            this.board.collectTiles(tiles);

            boosters.forEach((data: any) => {
                this.applyBoosterBasic(data.index, data.boosterType, data.boosterOptions);
            })
        }
    }

    /**
     * Отработать прикосновение к бустеру
     * @param tileIndex Позиция (индекс) на доске
     */
    touchBooster(tileIndex: number) {
        let tile = this.board.getCellByIndex(tileIndex);
        let boosterData = tile.getBoosterData();
        this.applyBooster(tileIndex, boosterData.boosterType, boosterData.boosterOptions);
    }

    /**
     * Применить гравитацию
     */
    applyGravity() {
        let movedTiles = this.board.applyGravity();
        if (movedTiles.length > 0) {
            this.boardView.updateLabelBoard();
            setTimeout(() => {
                movedTiles.forEach((movedTile: MovedTile) => {
                    this.boardView.onTileMoved(movedTile, true);
                })

                this.fillBoard();
            }, 300)
        } else {
            this.fillBoard();
        }
    }

    /**
     * Заполнить пустоты на доске новыми тайлами
     */
    fillBoard() {
        setTimeout(() => {
            let newTiles = this.board.fillBoard();
            if (newTiles.length > 0) {
                this.boardView.onTilesSpawned(newTiles);
            }
            setTimeout(() => {
                this.board.onFilled();
                this.checkForFinish();
                this.checkForShuffle();
            }, 400)

            this.boardView.updateLabelBoard();
        }, 300)
    }

    /**
     * Переключить режим отладки
     */
    switchDebugMode() {
        setDebugMode(!isDebugMode());
    }

    /**
     * Проверка на окончание уровня
     */
    checkForFinish() {
        if (this.board.getBoardState() == BoardState.FINISHED) {
            let message = (this.board.getScores() >= this.board.getRequiredScores()) ? "SUCCESSFUL!!!" : "FAILED!";
            this.boardView.cleanupBoard();
            this.showResultMessage(message);
        }
    }

    /**
     * Скрыть сообщение об окончании уровня
     */
    hideResult() {
        this.resultWindow.layer = Layers.Enum.NONE;
        this.resultWindowBackground.layer = Layers.Enum.NONE;
        this.resultWindowMessageLabel.node.layer = Layers.Enum.NONE;
    }

    /**
     * Показать сообщение об окончании уровня
     * @param message
     */
    showResultMessage(message: string) {
        this.resultWindow.layer = Layers.Enum.UI_2D;
        this.resultWindowBackground.layer = Layers.Enum.UI_2D;
        this.resultWindowMessageLabel.node.layer = Layers.Enum.UI_2D;

        this.resultWindowMessageLabel.string = message;

        let opacity = this.resultWindow.getComponent(UIOpacity) as UIOpacity;
        tween(opacity).to(0.5, { opacity: 255 }, {  easing: "quartOut" }).start();

        this.waitUserInputToEnd = true;
    }

    /**
     * Проверка, нужно ли выполнить перемешивание доски. Не выполняется, если на доске есть бустеры.
     */
    checkForShuffle() {
        if (this.board.getBoardState() == BoardState.IDLE && !this.board.isThereBooster() && !this.board.checkForMatches()) {
            if (this.board.shuffleBoard()) {
                this.boardView.updateBoardStateLabel();
                this.boardView.updateLabelBoard();
                this.boardView.onBoardShuffled();
            } else {
                this.boardView.cleanupBoard();
                this.showResultMessage("FAILED!\nNO TURNS AVAILABLE!");
            }
        }
    }
}
