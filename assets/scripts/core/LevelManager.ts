import * as levels from "../../configs/levels_config.json";

export type Item = {
    typeID: number,
    index: number,
}

export type Level = {
    id: number,
    width: number,
    height: number,
    moves: number,
    targetScores: number,
    items: Item[],
    randomTiles: number[],
}

export class LevelManager {
    constructor() {
    }

    /**
     * Получить конфиг указанного уровня
     * @param index Номер требуемого уровня
     * @returns Конфиг уровня
     */
    getLevel(index: number): Level {
        for (let level of levels.default) {
            if (level.id == index) {
                return level;
            }
        }

        return null;
    }

    /**
     * Получить номер максимального уровня
     * @returns номер максимального уровня
     */
    getMaxLevelID(): number {
        let id = 0;
        for (let level of levels.default) {
            if (id < level.id) {
                id = level.id;
            }
        }

        return id;
    }
}
