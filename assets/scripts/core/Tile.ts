import { BoosterType } from "./Boosters/Booster";

const COLORED = true;

export enum TileType {
    Blue, Green, Purple, Red, Yellow, Fish, Rocket, Bomb, MegaBomb,
}

/**
 * Используется для отладочного вывода тайлов
 */
let TileTypeShort = {
    [TileType.Blue]: "B",
    [TileType.Green]: "G",
    [TileType.Purple]: "P",
    [TileType.Red]: "R",
    [TileType.Yellow]: "Y",
    [TileType.Fish]: "-",
    [TileType.Rocket]: "2",
    [TileType.Bomb]: "3",
    [TileType.MegaBomb]: "4",
}

/**
 * Используется для окрашивания отладочного вывода в консоль
 */
let Colors = {
    [TileType.Blue]:    "\u001b[1;34m", // Blue
    [TileType.Green]:   "\u001b[1;32m", // Green
    [TileType.Purple]:  "\u001b[1;35m", // Purple
    [TileType.Red]:     "\u001b[1;31m", // Red
    [TileType.Yellow]:  "\u001b[1;33m", // Yellow
    [TileType.Fish]:  "\u001b[1;36m", // Cyan
    [TileType.Rocket]:  "\u001b[0m",
    [TileType.Bomb]:  "\u001b[0m",
    [TileType.MegaBomb]:  "\u001b[0m",
}

/**
 * Используется для окрашивания отладочного вывода на экран (RichText)
 */
let ColorsRich = {
    [TileType.Blue]:    "#00FFFF",
    [TileType.Green]:   "#00FF00",
    [TileType.Purple]:  "#FF00FF",
    [TileType.Red]:     "#FF4040",
    [TileType.Yellow]:  "#FFFF00",
    [TileType.Fish]:  "#00FFFF",
    [TileType.Rocket]:  "",
    [TileType.Bomb]:  "",
    [TileType.MegaBomb]:  "",
}

/**
 * Сколько очков выдаётся за каждый тайл
 */
let Scores = {
    [TileType.Blue]: 5,
    [TileType.Green]: 5,
    [TileType.Purple]: 5,
    [TileType.Red]: 5,
    [TileType.Yellow]: 5,
    [TileType.Fish]: 20,
    [TileType.Rocket]: 20,
    [TileType.Bomb]: 20,
    [TileType.MegaBomb]: 20,
}

/**
 * Какой бустер соответствует каждому тайлу
 */
let Boosters = {
    [TileType.Fish]: BoosterType.Fish,
    [TileType.Rocket]: BoosterType.Rocket,
    [TileType.Bomb]: BoosterType.Bomb,
    [TileType.MegaBomb]: BoosterType.MegaBomb,
}

/**
 * Модель тайла, находящегося на игровом поле.
 */
export class Tile {
    /** Уникальный идентификатор тайла */
    uniqueID: number;

    /** Тип тайла */
    typeID: number;

    /** Каким бустером является этот тайл */
    boosterType: BoosterType = null;

    /** Параметры бустера */
    boosterOptions: any = {};

    /** Индекс (координата) тайла на доске */
    index: number = null;

    /** Сколько очков даёт сбор этого тайла */
    scores: number;

    constructor(id: number, type: TileType) {
        this.uniqueID = id;
        this.typeID = type;
        this.scores = Scores[type];
        if (Boosters[type] != undefined) {
            this.boosterType = Boosters[type];
        }
    }

    /** Отладочный вывод */
    toString(): string {
        let id = this.uniqueID < 10 ? "0" + this.uniqueID : this.uniqueID;

        if (COLORED) {
            let color = Colors[this.typeID];
            return color + TileTypeShort[this.typeID] + id + "\u001b[0m";
        } else {
            return TileTypeShort[this.typeID] + id;
        }
    }

    /** Отладочный вывод (более подробный) */
    toStringDetailed(richText: boolean): string {
        let index = this.index < 10 ? "0" + this.index : this.index;
        if (richText) {
            let id = this.uniqueID < 10 ? "0" + this.uniqueID : this.uniqueID;
            let tileType = TileTypeShort[this.typeID];
            let color = ColorsRich[this.typeID];
            return `<b><color=${color}>${tileType}${id}</color></b><color=white>(${index})</color>`;
        } else {
            return `${this.toString()}(${index})`;
        }
    }

    /**
     * Получить данные о бустере
     * @returns данные о бустере
     */
    getBoosterData() {
        return {
            boosterType: this.boosterType,
            boosterOptions: this.boosterOptions,
        }
    }

    /**
     * Является ли тайл бустером
     * @returns true, если тайл является бустером, иначе false
     */
    isBooster(): boolean {
        return (this.boosterType != null);
    }
}
