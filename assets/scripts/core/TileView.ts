import { _decorator, Component, Label, Vec3, tween, UIOpacity, Layers } from 'cc';
import { isDebugMode } from './Utils';
const { ccclass, property } = _decorator;

/**
 * Изображение тайла (вид)
 */
@ccclass('TileView')
export class TileView extends Component {
    /** Уникальный ID тайла, к которому привязан этот TileView */
    private tileID: number = null;

    /** Индекс тайла, к которому привязан этот TileView */
    private tileIndex: number = null;

    update(deltaTime: number) {
        this.checkDebugMode();
    }


    /**
     * Задать индекс (позиция на доске) тайла, к которому привязан этот TileView
     * @param index Новый индекс
     */
    setTileIndex(index: number) {
        this.tileIndex = index;
        this.updateDebugData();
    }

    /**
     * Получить индекс тайла, к которому привязан этот TileView
     * @returns Индекс
     */
    getTileIndex(): number {
        return this.tileIndex;
    }

    /** Задать ID тайла, к которому привязан этот TileView */
    setTileID(tileID: number) {
        this.tileID = tileID;
        this.updateDebugData();
    }

    /**
     * Получить ID тайла, к которому привязан этот TileView
     * @returns ID тайла
     */
    getTileID() {
        return this.tileID;
    }

    /**
     * Задать позицию TileView на доске
     * @param x Координата X (в пикселях)
     * @param y Координата Y (в пикселях)
     */
    setPosition(x: number, y: number) {
        this.node.position.set(x, y);
    }

    /**
     * Обновление отладочной информации
     */
    updateDebugData() {
        if (this.tileID != null && this.tileIndex != null) {
            let label = this.node.children[0].getComponent("cc.Label") as Label;
            label.string = `ID: ${this.tileID}\ni:${this.tileIndex}`;
            this.checkDebugMode();
        }
    }

    /**
     * Переместить TileView в указанную позицию
     * @param x Координата X (в пикселях)
     * @param y Координата Y (в пикселях)
     * @param duration Время, за которое нужно переместиться
     * @param isFell Нужно ли проигрывать эффект отскока (используется при падении)
     */
    moveTo(x: number, y: number, duration: number, isFell: boolean) {
        tween(this.node).to(duration,
            { position: new Vec3(x, y, 0) },
            { easing: isFell ? "bounceOut" : "quadOut" },
        ).start();
    }

    /**
     * Проигрывание анимаций при удалении TileView
     */
    onDestroy() {
        let opacity = this.node.getComponent(UIOpacity) as UIOpacity;
        tween(opacity).to(0.5, { opacity: 0 }, {  easing: "quartOut" }).start();
        tween(this.node).to(0.5, { scale: new Vec3(0, 0, 0) }).start();
    }

    /**
     * Включение / выключение отладочной информации на тайлах в зависимости от состояния режима отладки
     */
    checkDebugMode() {
        let mode = isDebugMode();
        let label = this.node.children[0].getComponent("cc.Label") as Label;
        label.node.layer = mode ? Layers.Enum.UI_2D : Layers.Enum.NONE;
    }
}
