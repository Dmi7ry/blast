/**
 * Перемешивание массива случайным образом. Fisher–Yates shuffle
 * @param array
 */
export function shuffle<T>(array: T[]) {
    let m: number = array.length;
    let i: number;
    while (m) {
        i = Math.floor(Math.random() * m--);
        [array[m], array[i]] = [array[i], array[m]];
    }
}

/**
 * Получить случайное целое число в указанном диапазоне (от нуля)
 * @param range Максимальное допустимое значение
 * @returns Целое число в диапазоне (0...range)
 */
export function iRandom(range: number): number {
    return Math.round(Math.random() * range);
}

/**
 * Выбрать один из вариантов случайным образом
 * @param t Таблица со значениями
 * @returns Выбранное значение
 */
export function choose<T>(t: T[]): T {
    let i = Math.floor(Math.random() * t.length);
    return t[i];
}

/**
 * Изменить режим отладки
 * @param state Новое состояние
 */
export function setDebugMode(state: boolean) {
    globalThis.ProjectDebugSettings.isDebugMode = state;
    console.log("[Debug] Set debug mode: ", state ? "ON" : "OFF");
}

/**
 * Проверить, включен ли режим отладки
 * @returns true, если включен режим отладки, иначе false
 */
export function isDebugMode(): boolean {
    return globalThis.ProjectDebugSettings.isDebugMode;
}
