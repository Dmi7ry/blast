import { _decorator, Component, Slider, Label, director } from 'cc';
const { ccclass, property } = _decorator;

const DEFAULT_LEVEL = 1;
const MIN_LEVEL = 1;
const MAX_LEVEL = 3;

@ccclass('MainController')
export class MainController extends Component {
    // Номер уровня
    @property({type:Label})
    private levelCounterLabel = null;

    level: number = DEFAULT_LEVEL;

    private isMenuLocked: boolean = false;

    start() {
        console.log("[LOBBY] Start 3");
        console.log("Level: ", this.levelCounterLabel);

        this.levelCounterLabel.string = this.level;

        globalThis.ProjectDebugSettings = {};
    }

    OnStartPressed() {
        if (!this.isMenuLocked) {
            this.isMenuLocked = true;
            console.log("[LOBBY] Start pressed. Go to Core!");
            globalThis.ProjectDebugSettings.level = this.level;
            setTimeout(() => {
                director.loadScene("core");
            }, 100)
        }
    }

    OnSliderChanged(slider: Slider, customEventData: string) {
        let value = Math.round((MAX_LEVEL - MIN_LEVEL) * slider.progress) + MIN_LEVEL;
        this.level = value;
        this[customEventData].string = value;
    }
}
